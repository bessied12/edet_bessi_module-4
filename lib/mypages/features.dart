import 'package:farm_tracking_app/mypages/login_dashboard.dart';
import 'package:flutter/material.dart';

import '../main.dart';

void main() {
  runApp(const MyApp());
}

class Gallery extends StatefulWidget {
  const Gallery({Key? key}) : super(key: key);
  @override
  // ignore: library_private_types_in_public_api
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: const Text('G A L L E R Y '),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color.fromARGB(255, 9, 29, 2),
            ),
            child: Column(
              children: [
                Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      alignment: Alignment.center,
                      height: 150,
                      width: 150,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: const Icon(
                        Icons.photo_album_rounded,
                        size: 150,
                        color: Colors.white,
                      ),
                    )),
                Expanded(
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            height: 400,
                            width: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              image: const DecorationImage(
                                  image: AssetImage('farm1.png')),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            height: 400,
                            width: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              image: const DecorationImage(
                                  image: AssetImage('farm2.png')),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            height: 400,
                            width: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              image: const DecorationImage(
                                  image: AssetImage('farm3.png')),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            height: 400,
                            width: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              image: const DecorationImage(
                                  image: AssetImage('farm4.png')),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            height: 400,
                            width: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              image: const DecorationImage(
                                  image: AssetImage('farm5.png')),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            height: 400,
                            width: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              image: const DecorationImage(
                                  image: AssetImage('fram6.png')),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            height: 400,
                            width: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              image: const DecorationImage(
                                  image: AssetImage('farm7.png')),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            height: 400,
                            width: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              image: const DecorationImage(
                                  image: AssetImage('farm8.png')),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            height: 400,
                            width: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              image: const DecorationImage(
                                  image: AssetImage('farm9.png')),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            height: 400,
                            width: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              image: const DecorationImage(
                                  image: AssetImage('farm10.png')),
                            ),
                          )),
                      const SizedBox(
                        height: 50,
                      )
                    ],
                  ),
                ),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Container(
                              height: 40,
                              width: 140,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  color: Colors.black),
                              child: OutlinedButton.icon(
                                  icon: const Text('Dashboard'),
                                  label: const Icon(Icons.dashboard_customize),
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              const Dashboard()))))),
                      Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Container(
                              height: 40,
                              width: 140,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  color: Colors.black),
                              child: OutlinedButton.icon(
                                  icon: const Text('Products'),
                                  label: const Icon(
                                      Icons.production_quantity_limits_rounded),
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              const Products()))))),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}

class Products extends StatefulWidget {
  const Products({Key? key}) : super(key: key);
  @override
  // ignore: library_private_types_in_public_api
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: const Text('P R O D U C T S'),
          centerTitle: true,
        ),
        body: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: const Color.fromARGB(255, 9, 29, 2),
              ),
              child: Column(children: [
                Expanded(
                    child:
                        ListView(scrollDirection: Axis.horizontal, children: [
                  Padding(
                      padding: const EdgeInsets.all(50.0),
                      child: Container(
                        padding: const EdgeInsets.all(15.0),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Column(children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.asset(
                              'farm4.png',
                              height: 250,
                              width: 400,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          const SizedBox(height: 10),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color:
                                      const Color.fromARGB(255, 224, 213, 213),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                alignment: Alignment.center,
                                width: 50,
                                height: 20,
                                child: const Text(
                                  'MAIZE',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                )),
                          ),
                        ]),
                      )),
                  Padding(
                      padding: const EdgeInsets.all(50.0),
                      child: Container(
                        padding: const EdgeInsets.all(15.0),
                        width: 450,
                        height: 200,
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Column(children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.asset(
                              'farm3.png',
                              height: 250,
                              width: 400,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          const SizedBox(height: 10),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color:
                                      const Color.fromARGB(255, 224, 213, 213),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                alignment: Alignment.center,
                                width: 100,
                                height: 20,
                                child: const Text(
                                  'CHICKENS',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                )),
                          ),
                        ]),
                      )),
                  Padding(
                      padding: const EdgeInsets.all(50.0),
                      child: Container(
                        padding: const EdgeInsets.all(15.0),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Column(children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.asset(
                              'farm7.png',
                              height: 250,
                              width: 400,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          const SizedBox(height: 10),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color:
                                      const Color.fromARGB(255, 224, 213, 213),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                alignment: Alignment.center,
                                width: 120,
                                height: 20,
                                child: const Text(
                                  'VEGETABLES',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                )),
                          ),
                        ]),
                      )),
                  Padding(
                      padding: const EdgeInsets.all(50.0),
                      child: Container(
                        padding: const EdgeInsets.all(15.0),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Column(children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.asset(
                              'fram6.png',
                              height: 250,
                              width: 400,
                              fit: BoxFit.fill,
                            ),
                          ),
                          const SizedBox(height: 10),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color:
                                      const Color.fromARGB(255, 224, 213, 213),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                alignment: Alignment.center,
                                width: 50,
                                height: 20,
                                child: const Text(
                                  'COWS',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                )),
                          ),
                        ]),
                      )),
                ])),
                Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Container(
                            height: 40,
                            width: 140,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: const Color.fromARGB(255, 10, 4, 94)),
                            child: OutlinedButton.icon(
                                icon: const Text(
                                  'Dashboard',
                                  style: TextStyle(color: Colors.white),
                                ),
                                label: Icon(
                                  Icons.dashboard_customize,
                                  color: Colors.black,
                                ),
                                onPressed: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            const Dashboard()))))),
                    Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Container(
                            height: 40,
                            width: 140,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: const Color.fromARGB(255, 3, 29, 114)),
                            child: OutlinedButton.icon(
                                icon: const Text(
                                  'Gallery',
                                  style: TextStyle(
                                      color:
                                          Color.fromARGB(255, 250, 250, 249)),
                                ),
                                label: const Icon(
                                  Icons.production_quantity_limits_rounded,
                                  color: Colors.black,
                                ),
                                onPressed: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            const Gallery()))))),
                  ],
                )),
                const SizedBox(height: 70)
              ]),
            )));
  }
}
