import 'package:farm_tracking_app/mypages/login_dashboard.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Farm Tracker App',
      theme: ThemeData(
          primaryColorDark: Colors.cyan[900],
          scaffoldBackgroundColor: Colors.black,
          primarySwatch: Colors.indigo),
      home: const LoginPage(),
    );
  }
}
